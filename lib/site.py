from lib import config, graficos, static

import plotly.plotly as py
from plotly.graph_objs import *
import dash
import dash_core_components as dcc
import dash_html_components as html
import os
import glob
from threading import Thread
from flask import send_from_directory


class siteApp(Thread):
    def __init__(self):
        super().__init__()

    def run(self):
        app = dash.Dash()
        
        app.css.config.serve_locally = True
        app.scripts.config.serve_locally = True

        def homepage():
            
            #adicionar aqui uma varial que armazena o valor
            lista = glob.glob('static/*')
            #print(lista)
            #ultimo = max(lista, key=os.path.getctime)
            #print(ultimo)
            
            return html.Div([
                html.Link(rel='stylesheet', href='/static/stylesheet.css'),
                html.Img(src='/static/banner.png'),
                html.Img(src='/static/a.jpg'),
                #html.Div([html.Img(src='/static/a.jpg', className='imag', style={'display': 'block'; 'margin': 'auto'})], className='imag'),
                # html.Div([html.Img(src='/static/a.jpg')], className='imag', style={"align": "center"}),
                html.Div([
                    html.Button('Ascender / Apagar Luz', id='luz'),
                    html.Button('Capturar Imagem', id='imagem'),
                    ], id='botoes'),
                html.Div(children=graficos.linha('Temperatura'), className='g1'),
                html.Div(children=graficos.linha('Umidade'), className='g2'),
                html.Div(children=graficos.linha('Luminosidade'), className='g3'),
                html.Div(children=graficos.linha('Extra'), className='g4')
            ], className='wrapper')

        app.layout = homepage()
        
        @app.server.route('/static/<resource>')
        def serve_static(resource):
            return send_from_directory(config.ARQ['static'], resource)

        app.run_server(host='0')
